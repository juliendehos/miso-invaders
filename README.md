# miso-invaders

Space-invaders in Haskell, using [Miso](https://github.com/dmjio/miso).

[Play online !](https://juliendehos.gitlab.io/miso-invaders)


![](archive/miso-invaders.mp4)


How to build:

```
make
firefox public/index.html
```

